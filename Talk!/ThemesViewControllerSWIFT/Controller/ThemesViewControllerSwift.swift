//
//  ThemesViewControllerSwift.swift
//  Talk!
//
//  Created by Роман Орлов on 14/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

class ThemesViewControllerSwift: UIViewController, UIBarPositioningDelegate {
    
    // MARK: Outlets
    @IBOutlet var firstButton: UIButton!
    @IBOutlet var secondButton: UIButton!
    @IBOutlet var thirdButton: UIButton!
    @IBOutlet var navBar: UINavigationBar!
    
    // MARK: Properties
    var buttons = [UIButton]()
    var themes = ThemesSwift()
    var themesViewControllerDelegateClosure: ((_ didSelectTheme: UIColor)->())?
    var dataManager: DataManager?
    
    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        dataManager = GCDDataManager()
        themes = ThemesSwift()
        themes.theme1 = UIColor.white
        themes.theme2 = UIColor.creme
        themes.theme3 = UIColor.windows
        DispatchQueue.main.async { [unowned self] in
            if let color = self.dataManager?.retrieveColor(forKey: "Theme") {
                self.view.backgroundColor = color
                self.navBar.barTintColor = color
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        buttons = [firstButton, secondButton, thirdButton]
        for button in buttons {
            setButtonAppearance(button)
        }
    }
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.topAttached
    }
    
    // MARK: Actions
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func themeButtonClicked(_ sender: Any) {
        let button = sender as! UIButton
        switch button.tag {
        case 1:
            chooseTheme(themes.theme1 ?? .white)
        case 2:
            chooseTheme(themes.theme2 ?? .white)
        case 3:
            chooseTheme(themes.theme3 ?? .white)
        default:
            break;
        }
    }
    
    // MARK: Private methods
    private func setButtonAppearance(_ button: UIButton) {
        button.layer.cornerRadius = button.frame.size.height/2;
        button.layer.shadowOpacity = 0.1;
        button.layer.shadowColor = UIColor.black.cgColor;
        button.layer.shadowRadius = 7;
        button.layer.shadowOffset = CGSize(width: 0, height: 3);
    }
    private func chooseTheme(_ theme: UIColor) {
        dataManager = GCDDataManager()
        dataManager?.saveColor(theme, forKey: "Theme")
        self.view.backgroundColor = theme
        navBar.barTintColor = theme;
        UINavigationBar.appearance().barTintColor = theme
        themesViewControllerDelegateClosure?(theme)
    }
}
