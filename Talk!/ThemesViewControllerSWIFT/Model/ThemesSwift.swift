//
//  ThemesSwift.swift
//  Talk!
//
//  Created by Роман Орлов on 14/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

class ThemesSwift {
    // Я не стал переопределять геттеры/сеттеры, ибо незачем.
    var theme1: UIColor?
    var theme2: UIColor?
    var theme3: UIColor?
}
