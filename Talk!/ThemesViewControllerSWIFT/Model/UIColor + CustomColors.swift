//
//  UIColor + CustomColors.swift
//  Talk!
//
//  Created by Роман Орлов on 14/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

extension UIColor {
    class var creme: UIColor {
        return UIColor(red: 248/255, green: 236/255, blue: 199/255, alpha: 1)
    }
    class var windows: UIColor {
        return UIColor(red: 72/255, green: 136/255, blue: 204/255, alpha: 1)
    }
}
