//
//  AppState.swift
//  Talk!
//
//  Created by Роман Орлов on 21/09/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

// Данная функция превращает rawValues из энума UIApplication.State в соответствующие им строки.

public func state(_ input: UIApplication.State) -> String {
    
    var currentState: String = "not defined"
    
    if input.rawValue == 0 {
        currentState = "active"
    }
    else if input.rawValue == 1 {
        currentState = "inactive"
    }
    else if input.rawValue == 2 {
        currentState = "background"
    }
    return currentState
}
