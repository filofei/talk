//
//  DebugOnlyPrint.swift
//  Talk!
//
//  Created by Роман Орлов on 20/09/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

var inDebugMode: Bool = false

public func printDebug(_ items: Any) {
    if inDebugMode {
    Swift.print(items)
    }
}
