//
//  DataManager.swift
//  Talk!
//
//  Created by Роман Орлов on 19/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

protocol DataManager {    
    func saveImage(_ image: UIImage, named: String) -> Bool
    func saveImage(_ image: UIImage, named: String, completion: (() -> Void)) -> Bool
    func save(_ object: Any?, forKey: String)
    func save(_ object: Any?, forKey: String, completion: (() -> Void))
    func saveColor(_ color: UIColor?, forKey key: String)
    func saveColor(_ color: UIColor?, forKey key: String, completion: (() -> Void))
    func retrieveColor(forKey key: String) -> UIColor?
    func retrieveImage(named: String) -> UIImage?
    func retrieve(forKey: String) -> Any?
}
