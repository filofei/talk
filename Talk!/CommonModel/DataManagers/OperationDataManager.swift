//
//  OperationDataManager.swift
//  Talk!
//
//  Created by Роман Орлов on 18/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

final class OperationDataManager: BlockOperation, DataManager {
    
    let operationQueue = OperationQueue()
    let fileManager = FileManager()
    
    func saveImage(_ image: UIImage, named: String) -> Bool {
        var result = false
        operationQueue.addOperation { [unowned self] in
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(named)
            let imageData = image.jpegData(compressionQuality: 0.5)
            if self.fileManager.createFile(atPath: path, contents: imageData, attributes: nil) {
                result = true
            } else {
                result = false
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        return result
    }
    
    func saveImage(_ image: UIImage, named: String, completion: (() -> Void)) -> Bool {
        var result = false
        operationQueue.addOperation { [unowned self] in
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(named)
            let imageData = image.jpegData(compressionQuality: 0.5)
            if self.fileManager.createFile(atPath: path, contents: imageData, attributes: nil) {
                result = true
            } else {
                result = false
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        completion()
        return result
    }
    
    func saveColor(_ color: UIColor?, forKey key: String) {
        operationQueue.addOperation {
            UserDefaults.standard.setColor(color: color, forKey: key)
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }

    func save(_ object: Any?, forKey key: String) {
        operationQueue.addOperation {
            UserDefaults.standard.set(object, forKey: key)
        }
        operationQueue.waitUntilAllOperationsAreFinished()
    }
    
    func saveColor(_ color: UIColor?, forKey key: String, completion: (() -> Void)) {
        operationQueue.addOperation {
            UserDefaults.standard.setColor(color: color, forKey: key)
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        completion()
    }
    
    func retrieveColor(forKey key: String) -> UIColor? {
        var color: UIColor?
        operationQueue.addOperation {
            color = UserDefaults.standard.colorForKey(key: key)
        }
        return color
    }

    
    func save(_ object: Any?, forKey key: String, completion: (() -> Void)) {
        operationQueue.addOperation {
            UserDefaults.standard.set(object, forKey: key)
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        completion()
    }
    
    override func main() {
        super.main()
    }
    func retrieve(forKey key: String) -> Any? {
        var data: Any?
        operationQueue.addOperation {
            data = UserDefaults.standard.value(forKey: key)
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        return data
    }
    func retrieveImage(named: String) -> UIImage? {
        var imageData: UIImage?
        operationQueue.addOperation { [unowned self] in
            let directoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let imagePath = (directoryPath as NSString).appendingPathComponent(named)
            if self.fileManager.fileExists(atPath: imagePath) {
                imageData = UIImage(contentsOfFile: imagePath)
            } else {
                print("Error occured while getting image at specified path.")
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        return imageData
    }
}
