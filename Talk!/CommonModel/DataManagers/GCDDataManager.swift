//
//  GCDDataManager.swift
//  Talk!
//
//  Created by Роман Орлов on 18/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

final class GCDDataManager: DataManager {
    
    private let group = DispatchGroup()
    private let globalQueue = DispatchQueue.global()
    private let fileManager = FileManager.default
    
    /// Asynchronously saves UIImage in User Defaults with completion handler
    func saveImage(_ image: UIImage, named: String, completion: (() -> Void)) -> Bool {
        var result = false
        group.enter()
        globalQueue.async { [unowned self] in
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(named)
            let imageData = image.jpegData(compressionQuality: 0.5)
            if self.fileManager.createFile(atPath: path, contents: imageData, attributes: nil) {
                result = true
            } else {
                result = false
            }
            self.group.leave()
        }
        group.wait()
        completion()
        return result
    }
    
    /// Asynchronously saves UIImage in User Defaults
    func saveImage(_ image: UIImage, named: String) -> Bool {
        var result = false
        globalQueue.async { [unowned self] in
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(named)
            let imageData = image.jpegData(compressionQuality: 0.5)
            if self.fileManager.createFile(atPath: path, contents: imageData, attributes: nil) {
                result = true
            } else {
                result = false
            }
        }
        return result
    }
    
    /// Asynchronously saves сolor in User Defaults with completion handler
    func saveColor(_ color: UIColor?, forKey key: String, completion: (() -> Void)) {
        group.enter()
        globalQueue.async { [unowned self] in
            UserDefaults.standard.setColor(color: color, forKey: key)
            self.group.leave()
        }
        group.wait()
        completion()
    }
    /// Asynchronously saves сolor in User Defaults
    func saveColor(_ color: UIColor?, forKey key: String) {
        globalQueue.async {
            UserDefaults.standard.setColor(color: color, forKey: key)
        }
    }
    
    /// Asynchronously saves object in User Defaults with completion handler
    func save(_ object: Any?, forKey key: String, completion: (() -> Void)) {
        group.enter()
        globalQueue.async { [unowned self] in
            UserDefaults.standard.set(object, forKey: key)
            self.group.leave()
        }
        group.wait()
        completion()
    }
    
    /// Asynchronously saves object in User Defaults
    func save(_ object: Any?, forKey key: String) {
        globalQueue.async {
            UserDefaults.standard.set(object, forKey: key)
        }
    }
    /// Asynchronously retrieves data from User Defaults
    func retrieve(forKey key: String) -> Any? {
        var data: Any?
        group.enter()
        globalQueue.async { [unowned self] in
            data = UserDefaults.standard.value(forKey: key)
            self.group.leave()
        }
        group.wait()
        return data
    }
    /// Asynchronously retrieves color data from User Defaults
    func retrieveColor(forKey key: String) -> UIColor? {
        var color: UIColor?
        group.enter()
        globalQueue.async { [unowned self] in
            color = UserDefaults.standard.colorForKey(key: key)
            self.group.leave()
        }
        group.wait()
        return color
    }
    /// Asynchronously retrieves image data from File Manager
    func retrieveImage(named: String) -> UIImage? {
        var imageData: UIImage?
        group.enter()
        globalQueue.async { [unowned self] in
            let directoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let imagePath = (directoryPath as NSString).appendingPathComponent(named)
            if self.fileManager.fileExists(atPath: imagePath) {
                imageData = UIImage(contentsOfFile: imagePath)
            } else {
                print("Error occured while getting image at specified path.")
            }
            self.group.leave()
        }
        group.wait()
        return imageData
    }

}
