//
//  ErrorList.swift
//  Talk!
//
//  Created by Роман Орлов on 21/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

enum ErrorList: Error {
    case NoSuchDirectory
    case ImageSavingFail
}
