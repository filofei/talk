//
//  Appearance.swift
//  Talk!
//
//  Created by Роман Орлов on 27/09/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

final class Appearance {
    
    private let defaultCornerRadius = CGFloat(35)
    private let defaultBorderWidth = CGFloat(2)
    
    public func setCornerRadius(_ view: UIView, _ radius: CGFloat? = nil) {
        view.layer.cornerRadius = radius ?? defaultCornerRadius
        view.layer.masksToBounds = true
    }
    
    /// Don't call this method in ViewDidLoad.
    public func makeRound(_ view: UIView) {
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.masksToBounds = true
    }
    
    public func setBorder(_ view: UIView,  _ width: CGFloat? = nil, _ color: CGColor? = nil) {
        view.layer.borderWidth = width ?? defaultBorderWidth
        view.layer.borderColor = color ?? UIColor.black.cgColor
    }
}
