//
//  CommunicatorDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 25/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

protocol CommunicatorDelegate:  class{
    
    // Discovering
    func didFoundUser(userID: String, userName: String?)
    func didLostUser(userID: String)
    
    // Errors
    func failedToStartBrowsingForUsers(error: Error)
    func failedToStartAdvertising(error: Error)
    
    // Messages
    func didRecieveMessage(text: String, fromUser: String, toUser: String)
}
