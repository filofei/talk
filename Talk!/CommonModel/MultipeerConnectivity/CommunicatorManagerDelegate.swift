//
//  CommunicatorManagerDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 26/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

protocol CommunicatorManagerDelegate: class{
    func didGetNewMessage(text: String, fromUser: String, toUser: String)
    func sendNewMessage(text: String, fromUser: String, toUser: String)
    func didFindUser(userID: String, userName: String)
    func didLoseUser(userID: String)
}
