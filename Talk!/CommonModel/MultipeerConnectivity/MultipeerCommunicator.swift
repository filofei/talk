//
//  MultipeerCommunicator.swift
//  Talk!
//
//  Created by Роман Орлов on 25/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import MultipeerConnectivity

class MultipeerCommunicator: NSObject, Communicator, MCSessionDelegate, MCNearbyServiceBrowserDelegate, MCNearbyServiceAdvertiserDelegate {
    
    let id: MCPeerID
    let serviceType: String
    let info: [String:String]
    let session: MCSession
    let advertiser: MCNearbyServiceAdvertiser
    let browser: MCNearbyServiceBrowser
    var sessions: [String: MCSession] = [:]
    
    weak var delegate: CommunicatorDelegate?
    var online: Bool = false
    
    override init() {
        self.id = MCPeerID(displayName: "Filofei")
        self.serviceType = "tinkoff-chat"
        self.info = ["userName":"Роман"]
        self.session = MCSession(peer: id)
        self.advertiser = MCNearbyServiceAdvertiser(peer: id, discoveryInfo: info, serviceType: "tinkoff-chat")
        self.browser = MCNearbyServiceBrowser(peer: id, serviceType: "tinkoff-chat")
        super.init()
        //self.session.delegate = self
        self.advertiser.delegate = self
        self.advertiser.startAdvertisingPeer()
        self.browser.delegate = self
        self.browser.startBrowsingForPeers()
    }
    deinit {
        self.advertiser.stopAdvertisingPeer()
        self.browser.stopBrowsingForPeers()
    }
    
    func generateMessageID() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate) + \(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        print(state.rawValue)
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        print("Did recieve some data: \(data)")
        let message = String(data: data, encoding: .utf8)!
        delegate?.didRecieveMessage(text: message, fromUser: peerID.displayName, toUser: id.displayName)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        print("Couldn't start browsing with error: \(error)")
        delegate?.failedToStartBrowsingForUsers(error: error)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        let session = sessionWith(peer: peerID)
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 20)
        delegate?.didFoundUser(userID: peerID.displayName, userName: info?["userName"])
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        print("Did lose peer: \(peerID.displayName)")
        // FIXME: Проверить работоспособность
        if sessions[peerID.displayName] != nil {
            sessions[peerID.displayName] = nil
        }
        delegate?.didLostUser(userID: peerID.displayName)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        print("Did Reсieve Invitation")
        if session.connectedPeers.contains(peerID) {
            print("Did Not Accept Invitation")
            invitationHandler(false, nil)
        } else {
            print("Did Accept Invitation")
            let session = sessionWith(peer: peerID)
            invitationHandler(true, session)
        }
    }
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        print("Couldn't start advertising with error: \(error)")
        delegate?.failedToStartAdvertising(error: error)
    }
    
    func sendMessage(string: String, to userID: String, completionHandler: ((Bool, Error?) -> ())?) {
        if let session = sessions[userID] {
            let peers = session.connectedPeers.filter { peer in
                peer.displayName == userID
            }
            do {
                try session.send(string.data(using: .utf8)!, toPeers: peers, with: .reliable)
                completionHandler?(true, nil)
            }
            catch let error {
                print(error)
            }
            
        } else {
            completionHandler?(false, nil)
            print("No session: \(userID)")
        }
    }
    func sessionWith(peer: MCPeerID) -> MCSession {
        if let session = sessions[peer.displayName] {
            return session
        } else {
            let session = MCSession(peer: id,
                                    securityIdentity: nil,
                                    encryptionPreference: .none)
            session.delegate = self
            sessions[peer.displayName] = session
            return sessions[peer.displayName]!
        }
    }
}
