//
//  CommunicatorManager.swift
//  Talk!
//
//  Created by Роман Орлов on 25/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

class CommunicatorManager: CommunicatorDelegate {
    
    var delegate: CommunicatorManagerDelegate?
    var communicator: MultipeerCommunicator?
    
    init() {
        communicator = MultipeerCommunicator()
        communicator?.delegate = self
    }
    
    func didFoundUser(userID: String, userName: String?) {
        delegate?.didFindUser(userID: userID, userName: userName ?? "NO_USERNAME")
    }
    
    func didLostUser(userID: String) {
        self.delegate?.didLoseUser(userID: userID)
    }
    
    func failedToStartBrowsingForUsers(error: Error) {
        print(error.localizedDescription)
    }
    
    func failedToStartAdvertising(error: Error) {
        print(error.localizedDescription)
    }
    
    func didRecieveMessage(text: String, fromUser: String, toUser: String) {
        self.delegate?.didGetNewMessage(text: text, fromUser: fromUser, toUser: toUser)
    }
    
    
}
