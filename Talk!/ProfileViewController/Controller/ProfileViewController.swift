//
//  ProfileViewController.swift
//  Talk!
//
//  Created by Роман Орлов on 26/09/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIBarPositioningDelegate {
    
    // MARK: Outlets
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var operationButton: UIButton!
    @IBOutlet var gcdButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userDescriptionLabel: UILabel!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet weak var userDescrptionTextView: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    // MARK: Properties
    let notificationCenter = NotificationCenter.default
    let appearance = Appearance()
    var chosenImage: UIImage?
    lazy var cameraPicker = UIImagePickerController()
    lazy var galleryPicker = UIImagePickerController()
    var dataManager: DataManager?
    var isInEditingMode = false
    var savingIsInProgress = false {
        didSet {
            if savingIsInProgress {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
        }
    }
    /// Indicates which fields have been changed and can be saved
    var pendForChanges = [0:false, 1:false, 2:false]
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self
        userDescrptionTextView.delegate = self
        appearance.setCornerRadius(profileImage)
        appearance.setBorder(editButton)
        setButtonsAppearance()
        setInitialScreenState()
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appearance.makeRound(addPhotoButton)
        appearance.setCornerRadius(editButton, 10)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    // MARK: Actions
    @IBAction func addPhotoTouched(_ sender: UIButton) {
        configurePickers()
        createAlertController()
    }
    @IBAction func editButtonClicked(_ sender: UIButton) {
        isInEditingMode.toggle()
        switchEditingMode()
    }
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        savingIsInProgress = true
        self.userDescrptionTextView.resignFirstResponder()
        self.usernameTextField.resignFirstResponder()
        switch sender.tag {
        case 1:
            dataManager = GCDDataManager()
            saveData(dataManager!)
        case 2:
            dataManager = OperationDataManager()
            saveData(dataManager!)
        default:
            print("Error in buttons' tags")
        }
        isInEditingMode = false
        switchEditingMode()
    }
    
    @IBAction func closeViewController(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.topAttached
    }
    
    // MARK: Private methods
    private func createAlertController() {
        
        let alert = UIAlertController(title: "Выберите источник", message: "Камера или галерея, вот в чём вопрос.", preferredStyle: .actionSheet)
        // Camera
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraAction = UIAlertAction(title: "Камера", style: .default, handler: { [unowned self] (_) in
                self.present(self.cameraPicker, animated: true, completion: nil)
            })
            alert.addAction(cameraAction)
        } else {
            alert.message = "Конечно, только галерея. Откуда камера на симуляторе?"
        }
        // Gallery
        let galleryAction = UIAlertAction(title: "Галерея", style: .default, handler: { [unowned self] (_) in
            self.present(self.galleryPicker, animated: true, completion: nil)
        })
        alert.addAction(galleryAction)
        // Cancel
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func configurePickers() {
        // Camera picker
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            cameraPicker.delegate = self
            cameraPicker.allowsEditing = false
            cameraPicker.sourceType = .camera
            cameraPicker.modalPresentationStyle = .fullScreen
            cameraPicker.cameraCaptureMode = .photo
        }
        
        // Gallery picker
        galleryPicker.delegate = self
        galleryPicker.allowsEditing = false
        galleryPicker.sourceType = .photoLibrary
    }
    
    /// Makes initial settings for save button.
    private func setButtonsAppearance() {
        let saveButtons = [gcdButton, operationButton]
        saveButtons.forEach { button in
            appearance.setBorder(button!)
            appearance.setCornerRadius(button!, 10)
            changeButtonState(button!)
        }
    }
    
    /// Makes initial settings to be called in ViewDidLoad.
    private func setInitialScreenState() {
        addPhotoButton.alpha = 0
        usernameTextField.alpha = 0
        userDescrptionTextView.alpha = 0
        userDescrptionTextView.textContainerInset = UIEdgeInsets(top: 3, left: 3, bottom: 2, right: 5)
        appearance.setBorder(userDescrptionTextView, 0.6, UIColor(white: 0.8, alpha: 1).cgColor)
        appearance.setCornerRadius(userDescrptionTextView, 5)
        // Loading user data from UserDefaults and File Manager
        DispatchQueue.main.async {
            let manager = GCDDataManager()
            guard let labelData = manager.retrieve(forKey: "Username") as? String else {
                return
            }
            self.usernameLabel.text = labelData
            guard let descriptionData = manager.retrieve(forKey: "UserDescription") as? String else {
                return
            }
            self.userDescriptionLabel.text = descriptionData
            guard let imageData = manager.retrieveImage(named: "UserPhoto.jpg") else {
                return
            }
            self.profileImage.image = imageData
        }
    }
    /// Switches Editing Mode.
    private func switchEditingMode() {        
        if isInEditingMode {
            editButton.setTitle("Отмена", for: .normal)
            setViewAlpha(view: addPhotoButton, alpha: 1)
            setViewAlpha(view: usernameTextField, alpha: 1)
            setViewAlpha(view: usernameLabel, alpha: 0)
            setViewAlpha(view: userDescrptionTextView, alpha: 1)
            setViewAlpha(view: userDescriptionLabel, alpha: 0)
            usernameTextField.text = usernameLabel.text
            userDescrptionTextView.text = userDescriptionLabel.text
        } else {
            let saveButtons = [gcdButton, operationButton]
            pendForChanges = [0:false, 1:false, 2:false]
            saveButtons.forEach { button in
                changeButtonState(button!)
            }
            editButton.setTitle("Редактировать", for: .normal)
            setViewAlpha(view: addPhotoButton, alpha: 0)
            setViewAlpha(view: usernameTextField, alpha: 0)
            setViewAlpha(view: usernameLabel, alpha: 1)
            setViewAlpha(view: userDescrptionTextView, alpha: 0)
            setViewAlpha(view: userDescriptionLabel, alpha: 1)
            userDescrptionTextView.resignFirstResponder()
            usernameTextField.resignFirstResponder()
        }
    }
    @objc private func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.bottomConstraint.constant -= keyboardHeight
            self.topConstraint.constant -= keyboardHeight
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    @objc private func keyboardWillHide(_ notification: Notification) {
        self.bottomConstraint.constant = -10
        self.topConstraint.constant = 10
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    /// Manages the state of save buttons depending on the textfield's content.
    @objc func textFieldDidChange(_ textField: UITextField) {
        let saveButtons = [gcdButton, operationButton]
        if textField.text != usernameLabel.text {
            saveButtons.forEach { button in
                pendForChanges[1] = true
                changeButtonState(button!)
            }
        } else {
            saveButtons.forEach { button in
                pendForChanges[1] = false
                changeButtonState(button!)
            }
        }
    }
    /// Changes state of button.
    func changeButtonState(_ button: UIButton) {
        switch pendForChanges {
        case [0:false, 1:false, 2:false]:
            button.isEnabled = false
            button.alpha = 0.3
        default:
            button.isEnabled = true
            button.alpha = 1
        }
    }
    /// Selects and saves user data to permanent storages.
    private func saveData(_ manager: DataManager) {
        var amountOfChangedFields = 0
        var amountOfSuccessfulFields = 0
        if pendForChanges[0] == true {
            amountOfChangedFields += 1
            if manager.saveImage(chosenImage!, named: "UserPhoto.jpg", completion: {[unowned self] in
                self.savingIsInProgress = false
                DispatchQueue.main.async { [unowned self] in
                    self.profileImage.image = manager.retrieveImage(named: "UserPhoto.jpg")
                }
            }) {
                amountOfSuccessfulFields += 1
            }
        }
        if pendForChanges[1] == true {
            amountOfChangedFields += 1
            manager.save(usernameTextField.text, forKey: "Username", completion: {[unowned self] in
                self.savingIsInProgress = false
                DispatchQueue.main.async { [unowned self] in
                    self.usernameLabel.text = manager.retrieve(forKey: "Username") as? String
                }
                amountOfSuccessfulFields += 1
            })
        }
        if pendForChanges[2] == true {
            amountOfChangedFields += 1
            manager.save(userDescrptionTextView.text, forKey: "UserDescription", completion: {[unowned self] in
                self.savingIsInProgress = false
                DispatchQueue.main.async { [unowned self] in
                    self.userDescriptionLabel.text = manager.retrieve(forKey: "UserDescription") as? String
                }
                amountOfSuccessfulFields += 1
            })
        }
        if amountOfChangedFields == amountOfSuccessfulFields {
            showSuccessAlert()
        } else {
            showFailAlert()
        }
    }
    /// Shows an alert with success message.
    private func showSuccessAlert() {
        let alert = UIAlertController(title: "Данные сохранены", message: "На сервер ФСБ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    /// Shows an alert with fail message and asks for saving data again.
    private func showFailAlert() {
        let alert = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Повторить", style: .default, handler: {[unowned self] (alert: UIAlertAction!) in
            self.saveData(self.dataManager!)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    /// Sets alpha for "inactive" state of buttons.
    private func setViewAlpha(view: UIView, alpha: CGFloat) {
        UIView.animate(withDuration: 0.3, animations: {
            view.alpha = alpha
        })
    }
    
}
