//
//  ProfileViewController + UIImagePickerControllerDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 05/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

extension ProfileViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        let saveButtons = [gcdButton, operationButton]
            saveButtons.forEach { button in
                pendForChanges[0] = true
                changeButtonState(button!)
            }
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
