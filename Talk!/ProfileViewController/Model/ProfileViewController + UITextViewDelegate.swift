//
//  ProfileViewController + UITextViewDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 18/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

extension ProfileViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 100
    }
    func textViewDidChange(_ textView: UITextView) {
        let saveButtons = [gcdButton, operationButton]
        if textView.text != userDescriptionLabel.text {
            saveButtons.forEach { button in
                pendForChanges[2] = true
                changeButtonState(button!)
            }
        } else {
            saveButtons.forEach { button in
                pendForChanges[2] = false
                changeButtonState(button!)
            }
        }
    }
}
