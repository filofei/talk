//
//  ProfileViewController + UITextFieldDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 18/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import Foundation

extension ProfileViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updatedText.count <= 20
    }
}
