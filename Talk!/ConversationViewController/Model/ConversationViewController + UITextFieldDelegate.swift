//
//  ConversationViewController + UITextFieldDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 07/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit
import MultipeerConnectivity

extension ConversationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != nil && textField.text != "" {
            // FIXME: Изменить способ отправки сообщения
        chatData.messages.append(textField.text!)
        }
        tableView.reloadData()
        self.scrollToBottom()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}
