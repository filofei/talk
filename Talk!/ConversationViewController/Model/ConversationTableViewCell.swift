//
//  ConversationTableViewCell.swift
//  Talk!
//
//  Created by Роман Орлов on 05/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

class ConversationTableViewCell: UITableViewCell, MessageCellConfiguration {
    
    // MARK: Outlets
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelBackgroundView: UIView!
    
    // MARK: Properties
    
    var messageText: String? {
        didSet {
            label.text = messageText
            configureViewAppearance(labelBackgroundView)
        }
    }
    
    // MARK: Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    private func configureViewAppearance(_ view: UIView) {
        view.layer.cornerRadius = 17
        view.layer.masksToBounds = true
    }

}
