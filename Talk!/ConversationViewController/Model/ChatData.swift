//
//  ChatData.swift
//  Talk!
//
//  Created by Роман Орлов on 05/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

struct ChatData {    
    var messages: [String] = []
}
