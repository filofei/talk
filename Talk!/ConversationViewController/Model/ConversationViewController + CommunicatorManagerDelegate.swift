//
//  ConversationViewController + CommunicatorManagerDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 27/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit
import MultipeerConnectivity

extension ConversationViewController: CommunicatorManagerDelegate {
    func didGetNewMessage(text: String, fromUser: String, toUser: String) {
        self.chatData.messages.append(text)
        tableView.reloadData()
    }
    
    func sendNewMessage(text: String, fromUser: String, toUser: String) {
        let somedata = text.data(using: String.Encoding.utf8)
        do {
            try manager?.communicator!.session.send(somedata!, toPeers: [MCPeerID.init(displayName: toUser)], with: MCSessionSendDataMode.reliable)
            print("Did send message")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func didFindUser(userID: String, userName: String) {
        
    }
    
    func didLoseUser(userID: String) {
        
    }
    
}
