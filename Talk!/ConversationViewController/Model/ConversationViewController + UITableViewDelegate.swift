//
//  ConversationViewController + UITableViewDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 05/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

extension ConversationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutgoingMessageCell") as! ConversationTableViewCell
            cell.messageText = chatData.messages[indexPath.row]
            return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatData.messages.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    
}
