//
//  ConversationViewController.swift
//  Talk!
//
//  Created by Роман Орлов on 04/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ConversationViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: Properties
    
    var chatData = ChatData()
    let notificationCenter = NotificationCenter.default
    
    var manager: CommunicatorManager?
    
    // MARK: Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
         manager = CommunicatorManager()
        self.textField.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        manager?.delegate = self
    }
    
    // MARK: Class methods
    
    @IBAction func tableViewDidTap(_ sender: UIGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.bottomConstraint.constant += keyboardHeight
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                self.scrollToBottom()
            })
        }
    }
    @objc private func keyboardWillHide(_ notification: Notification) {
        self.bottomConstraint.constant = 5
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.scrollToBottom()
        })
    }
    func scrollToBottom() {
        if tableView.numberOfRows(inSection: 0) > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: self.tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
}
