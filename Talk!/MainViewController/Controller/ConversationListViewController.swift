//
//  ConversationListViewController.swift
//  Talk!
//
//  Created by Роман Орлов on 19/09/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

class ConversationListViewController: UIViewController, ThemesViewControllerDelegate {
    
    func themesViewController(_ controller: ThemesViewController, didSelectTheme selectedTheme: UIColor) {
        logThemeChanging(selectedTheme: selectedTheme)
    }
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chooseThemeView: UIView!
    
    // MARK: Properties
    var data = UsersData()
    lazy var themesViewController: ThemesViewController? = nil
    var chooseThemeViewOpened = false
    var manager: CommunicatorManager?
    
    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CommunicatorManager()
        setChooseThemeViewAppearance(chooseThemeView)
        tableView.dataSource = self
        tableView.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        manager?.delegate = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if chooseThemeViewOpened {
            hideChooseThemeView(chooseThemeView)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToThemes" {
            let themes = segue.destination as? ThemesViewController
            themes?.delegate = self
        } else if segue.identifier == "ToThemesSwift" {
            let themesSwift = segue.destination as? ThemesViewControllerSwift
            themesSwift?.themesViewControllerDelegateClosure = {
                [weak self] selectedTheme in
                self?.logThemeChanging(selectedTheme: selectedTheme)
            }
        } else {
            super.prepare(for: segue, sender: sender)
        }
    }
    
    //MARK: Actions
    @IBAction func showChooseThemeView(_ sender: Any) {
        if !chooseThemeViewOpened {
            revealChooseThemeView(chooseThemeView)
        } else if chooseThemeViewOpened {
            hideChooseThemeView(chooseThemeView)
        }
    }
    
    // MARK: Private methods
    private func logThemeChanging(selectedTheme: UIColor) {
        print(selectedTheme)
    }
    
    private func revealChooseThemeView(_ view: UIView) {
        chooseThemeViewOpened.toggle()
        UIView.animate(withDuration: 0.7,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.35,
                       options: [.curveEaseInOut],
                       animations: {view.transform = CGAffineTransform.init(translationX: 0, y: 200)},
                       completion: nil)
    }
    private func hideChooseThemeView(_ view: UIView) {
        chooseThemeViewOpened.toggle()
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: [.curveEaseOut],
                       animations: {view.transform = CGAffineTransform.identity},
                       completion: nil)
    }
    
    private func setChooseThemeViewAppearance(_ view: UIView) {
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.15
        view.layer.shadowRadius = 8
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
    }
    
}

