//
//  ConversationListTableViewCell.swift
//  Talk!
//
//  Created by Роман Орлов on 04/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

class ConversationListTableViewCell: UITableViewCell, ConversationCellConfiguration {
    
    // MARK: Outlets
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    
    // MARK: Properties
    
    private let dateFormatter = DateFormatter()
    private var currentDate = Date()
    private var calendar = Calendar.current
    
    var name: String? {
        didSet {
            self.usernameLabel.text = name
        }
    }
    var message: String? {
        didSet {
            if message == nil {
                self.lastMessageLabel.font = UIFont.italicSystemFont(ofSize: 17)
                self.lastMessageLabel.textColor = UIColor(white: 0.65, alpha: 1)
                self.lastMessageLabel.text = "No messages yet"
            } else {
                self.lastMessageLabel.font = UIFont.systemFont(ofSize: 17)
                self.lastMessageLabel.textColor = UIColor(white: 0.65, alpha: 1)
                self.lastMessageLabel.text = message
            }
        }
    }
    var date: Date? = nil {
        didSet {
            dateFormatter.dateFormat = "dd.MM.yyy HH:mm"
            let defaultDate = dateFormatter.date(from: "01.01.1970 12:00")!
            
            let passedDay = calendar.component(.day, from: date ?? defaultDate)
            let currentDay = calendar.component(.day, from: currentDate)
            
            let passedMonth = calendar.component(.month, from: date ?? defaultDate)
            let currentMonth = calendar.component(.month, from: currentDate)
            
            let passedYear = calendar.component(.year, from: date ?? defaultDate)
            let currentYear = calendar.component(.year, from: currentDate)
            
            // Массивы с данными неплохо было бы сортировать по дате, но пока такого задания не было. 
            
            if (passedDay, passedMonth, passedYear) == (currentDay, currentMonth, currentYear) {
                dateFormatter.dateFormat = "HH:mm"
            } else if passedYear != currentYear {
                dateFormatter.dateFormat = "dd.MM.yyyy"
            } else {
                dateFormatter.dateFormat = "dd.MM"
            }
            dateLabel.text = dateFormatter.string(from: date ?? defaultDate)
        }
    }
    var online: Bool = false {
        didSet {
            if online {
                self.backgroundColor = UIColor(red: 255/255, green: 250/255, blue: 230/255, alpha: 1)
            } else {
                self.backgroundColor = .white
            }
        }
    }
    var hasUnreadMessages: Bool = false {
        didSet {
            if hasUnreadMessages && message != nil {
                self.lastMessageLabel.font = UIFont.boldSystemFont(ofSize: 17)
                self.lastMessageLabel.textColor = UIColor(white: 0.4, alpha: 1)
            }
        }
    }
    
    // MARK: Overridden methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
