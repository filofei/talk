//
//  UsersData.swift
//  Talk!
//
//  Created by Роман Орлов on 04/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class UsersData {
    
    // MARK: Data for cells
    
    public var userNames: [String:String] = [:] {
        didSet {
            sortedKeys = Array(userNames.keys).sorted{$0 < $1}
        }
    }
    public var userMessages: [String:String] = [:]
    public var userDates: [String:String] = [:]
    public var sortedKeys: [String] = []
    
    public let dateFormatter = DateFormatter()
    
    /// Sets data for each row in tableview with data from arrays.
    
    func setConversationListData(_ cell: ConversationListTableViewCell, _ row: Int, _ section: Int) -> ConversationListTableViewCell {
        
        // Message
        if !sortedKeys.isEmpty {
            cell.message = userMessages[sortedKeys[row]]
        }
        // Color
        switch section {
        case 0:
            cell.online = true
        case 1:
            cell.online = false
        default:
            print("Passed wrong section")
        }
        // Names
        if !sortedKeys.isEmpty {
            cell.name = userNames[sortedKeys[row]]
        }
        // Unread
        //        switch (section, row) {
        //        case (0...1, 0...3):
        //            cell.hasUnreadMessages = true
        //        default:
        //            cell.hasUnreadMessages = false
        //        }
        
        // Date        
        dateFormatter.dateFormat = "dd-MM-yyy HH:mm"
        if !userDates.isEmpty {
            let passedDate = dateFormatter.date(from: userDates[sortedKeys[row]]!)
            cell.date = passedDate
        }
        return cell
    }
}
