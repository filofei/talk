//
//  ConversationCellConfiguration.swift
//  Talk!
//
//  Created by Роман Орлов on 04/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

protocol ConversationCellConfiguration: class{
    var name: String? {get set}
    var message: String? {get set}
    var date: Date? {get set}
    var online: Bool {get set}
    var hasUnreadMessages: Bool {get set}
}
