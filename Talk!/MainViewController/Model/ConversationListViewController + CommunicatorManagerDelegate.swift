//
//  ConversationListViewController + CommunicatorManagerDelegate.swift
//  Talk!
//
//  Created by Роман Орлов on 26/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

import UIKit

extension ConversationListViewController: CommunicatorManagerDelegate {
    func didGetNewMessage(text: String, fromUser: String, toUser: String) {
        self.data.userMessages.updateValue(text, forKey: fromUser)
        self.data.userDates.updateValue(self.data.dateFormatter.string(from: Date()), forKey: fromUser)
    }
    
    func sendNewMessage(text: String, fromUser: String, toUser: String) {
        
    }
    
    func didFindUser(userID: String, userName: String) {
        self.data.userNames.updateValue(userName, forKey: userID)
        tableView.reloadData()
    }
    
    func didLoseUser(userID: String) {
        self.data.userNames.removeValue(forKey: userID)
        tableView.reloadData()
    }
    
}
