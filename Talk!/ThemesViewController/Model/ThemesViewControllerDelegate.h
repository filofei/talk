//
//  ThemesViewControllerDelegate.h
//  Talk!
//
//  Created by Роман Орлов on 12/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemesViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class ThemesViewController;

@protocol ThemesViewControllerDelegate <NSObject>
- (void)themesViewController:(ThemesViewController *)controller didSelectTheme:(UIColor *)selectedTheme;
@end

NS_ASSUME_NONNULL_END
