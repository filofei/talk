//
//  Themes.h
//  Talk!
//
//  Created by Роман Орлов on 11/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Themes : NSObject

- (void) setTheme1:(UIColor*)newTheme1;
- (void) setTheme2:(UIColor*)newTheme2;
- (void) setTheme3:(UIColor*)newTheme3;

- (UIColor*)theme1;
- (UIColor*)theme2;
- (UIColor*)theme3;

@end

NS_ASSUME_NONNULL_END
