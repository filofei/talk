//
//  Themes.m
//  Talk!
//
//  Created by Роман Орлов on 11/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

#import "Themes.h"
#import <UIKit/UIKit.h>

@implementation Themes
{
    UIColor *_theme1;
    UIColor *_theme2;
    UIColor *_theme3;
}

- (void) setTheme1:(UIColor *)newTheme1 {
    _theme1 = newTheme1;
    [newTheme1 release];
}
- (void) setTheme2:(UIColor *)newTheme2 {
    _theme2 = newTheme2;
    [newTheme2 release];
}
- (void) setTheme3:(UIColor *)newTheme3 {
    _theme3 = newTheme3;
    [newTheme3 release];
}

- (UIColor *) theme1 {
    return [_theme1 autorelease];
}
- (UIColor *) theme2 {
    return [_theme2 autorelease];
}
- (UIColor *) theme3 {
    return [_theme3 autorelease];
}

@end
