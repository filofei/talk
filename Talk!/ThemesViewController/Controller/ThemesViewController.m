//
//  ThemesViewController.m
//  Talk!
//
//  Created by Роман Орлов on 12/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

#import "ThemesViewController.h"

@interface ThemesViewController ()
- (void)setButtonAppearance:(UIButton*)button;
@end

@implementation ThemesViewController

// #MARK: Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // #MARK: Themes
    _themes = [[Themes alloc] init];
    [_themes setTheme1: UIColor.whiteColor];
    [_themes setTheme2: UIColor.lightGrayColor];
    [_themes setTheme3: UIColor.darkGrayColor];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    NSArray *buttons = @[_firstButton, _secondButton, _thirdButton];
    for (UIButton *button in buttons) {
        [self setButtonAppearance:button];
    }
}
- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (void)dealloc {
    [_firstButton release];
    [_secondButton release];
    [_thirdButton release];
    [_themes release];
    [_navBar release];
    [super dealloc];
}

// #MARK: IBActions

- (IBAction)closeButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)themeButtonClicked:(id)sender {
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 1:
            [self chooseTheme:[_themes theme1]];
            break;
        case 2:
            [self chooseTheme:[_themes theme2]];
            break;
        case 3:
            [self chooseTheme:[_themes theme3]];
            break;
        default:
            break;
    }
}

// #MARK: Private methods

- (void)setButtonAppearance:(UIButton*)button {
    button.layer.cornerRadius = button.frame.size.height / 2;
    button.layer.shadowOpacity = 0.1;
    button.layer.shadowColor = UIColor.blackColor.CGColor;
    button.layer.shadowRadius = 7;
    button.layer.shadowOffset = CGSizeMake(0, 3);
}
- (void)chooseTheme:(UIColor*)theme {
    self.view.backgroundColor = theme;    
    [_delegate themesViewController:self didSelectTheme:theme];
}

@end
