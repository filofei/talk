//
//  ThemesViewController.h
//  Talk!
//
//  Created by Роман Орлов on 12/10/2018.
//  Copyright © 2018 Роман Орлов. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Themes.h"
#import "ThemesViewControllerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ThemesViewController : UIViewController <UIBarPositioningDelegate>

@property (weak, nonatomic) id <ThemesViewControllerDelegate> delegate;
@property (weak, nonatomic) Themes *themes;

@property (retain, nonatomic) IBOutlet UIButton *firstButton;
@property (retain, nonatomic) IBOutlet UIButton *secondButton;
@property (retain, nonatomic) IBOutlet UIButton *thirdButton;
@property (retain, nonatomic) IBOutlet UINavigationBar *navBar;

- (IBAction)closeButtonClicked:(id)sender;

- (IBAction)themeButtonClicked:(id)sender;

@end

NS_ASSUME_NONNULL_END
